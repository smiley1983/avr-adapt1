EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X05 P1
U 1 1 57D256F9
P 5750 3700
F 0 "P1" H 5750 4000 50  0000 C CNN
F 1 "CONN_02X05" H 5750 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05" H 5800 3300 50  0000 C CNN
F 3 "" H 5750 2500 50  0000 C CNN
	1    5750 3700
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X03 P2
U 1 1 57D257D4
P 5750 4800
F 0 "P2" H 5750 5000 50  0000 C CNN
F 1 "CONN_02X03" H 5750 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 5750 4500 50  0000 C CNN
F 3 "" H 5750 3600 50  0000 C CNN
	1    5750 4800
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X03 P3
U 1 1 57D258D3
P 7700 4800
F 0 "P3" H 7700 5000 50  0000 C CNN
F 1 "CONN_02X03" H 7700 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 7700 4500 50  0000 C CNN
F 3 "" H 7700 3600 50  0000 C CNN
	1    7700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3500 6750 3500
Wire Wire Line
	6000 3600 6750 3600
Wire Wire Line
	6000 3700 6750 3700
Wire Wire Line
	6000 3800 6750 3800
Wire Wire Line
	6000 3900 6750 3900
Wire Wire Line
	5500 3500 4600 3500
Wire Wire Line
	5500 3600 4600 3600
Wire Wire Line
	5500 3700 4600 3700
Wire Wire Line
	5500 3800 4600 3800
Wire Wire Line
	5500 3900 4600 3900
Text Label 4750 3500 0    60   ~ 0
GND
Text Label 4750 3900 0    60   ~ 0
VCC
Text Label 6200 3500 0    60   ~ 0
MISO
Text Label 6200 3600 0    60   ~ 0
SCK
Text Label 6200 3700 0    60   ~ 0
RST
Text Label 6200 3900 0    60   ~ 0
MOSI
NoConn ~ 4600 3600
NoConn ~ 4600 3700
NoConn ~ 4600 3800
NoConn ~ 6750 3800
Wire Wire Line
	6000 4700 6550 4700
Wire Wire Line
	6000 4800 6550 4800
Wire Wire Line
	6000 4900 6550 4900
Wire Wire Line
	5500 4700 4900 4700
Wire Wire Line
	5500 4800 4900 4800
Wire Wire Line
	5500 4900 4900 4900
Text Label 6100 4700 0    60   ~ 0
VCC
Text Label 6100 4800 0    60   ~ 0
MOSI
Text Label 6100 4900 0    60   ~ 0
GND
Text Label 5050 4700 0    60   ~ 0
MISO
Text Label 5050 4800 0    60   ~ 0
SCK
Text Label 5050 4900 0    60   ~ 0
RST
Wire Wire Line
	8550 4700 7950 4700
Wire Wire Line
	8550 4800 7950 4800
Wire Wire Line
	8550 4900 7950 4900
Text Label 8100 4700 0    60   ~ 0
MISO
Text Label 8100 4800 0    60   ~ 0
SCK
Text Label 8100 4900 0    60   ~ 0
RST
Wire Wire Line
	6900 4700 7450 4700
Wire Wire Line
	6900 4800 7450 4800
Wire Wire Line
	6900 4900 7450 4900
Text Label 7000 4700 0    60   ~ 0
VCC
Text Label 7000 4800 0    60   ~ 0
MOSI
Text Label 7000 4900 0    60   ~ 0
GND
$EndSCHEMATC
